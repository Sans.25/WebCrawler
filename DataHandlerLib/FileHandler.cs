﻿// ***********************************************************************
// Assembly         : DataHandlerLib
// Author           : Anshul
// Created          : 02-24-2018
//
// Last Modified By : Anshul
// Last Modified On : 02-24-2018
// ***********************************************************************
// <copyright file="FileHandler.cs" company="DataHandlerLib">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DataHandlerLib
{
    public class FileHandler
    {
        /* private fields */
        private Uri uri = null;
        private string url = string.Empty;
        private string fileName = string.Empty;
        private FileStream fileStream = null;
        private FileAccess fileAccessMode = FileAccess.ReadWrite;

        private int dataChunkCount;
        private List<DataChunk> dataChunks = null;

        private List<DataChunkHandler> dataChunkHandlers = null;

        private UInt64 downloadRange = 0;
        private UInt64 contentLength = 0;

        /* public properties */
        public string Url = string.Empty;
        public string FileName => fileName;
        public FileStream FileStream => fileStream;
        public FileAccess FileAccessMode => fileAccessMode;
        public int DataChunkCount => dataChunkCount;
        public List<DataChunk> DataChunks => dataChunks;
        public List<DataChunkHandler> DataChunkHandlers => dataChunkHandlers;

        public FileHandler(string url)
        {
            // check if url is not empty.
            if (string.IsNullOrEmpty(url))
            {
                throw new ArgumentException("Empty Argument URL", nameof(url));
            }

            // check if url is valid.
            if (!Uri.TryCreate(url, UriKind.RelativeOrAbsolute, out uri))
            {
                throw new ArgumentException("Invalid URL", nameof(url));
            }

            this.url = uri.ToString();
        }

        internal bool getDownloadRange()
        {
            var request = HttpWebRequest.CreateHttp(uri);
            bool returnCode = false;

            request.Method = WebRequestMethods.Http.Head;

            //TODO set useragent..
            //request.UserAgent = 

            //TODO set referer..
            //request.Referer =

            // trace..
            Debug.WriteLine("Call GetResponse Head .. {0}", request.RequestUri);

            var response = request.GetResponse();

            if (response.SupportsHeaders
                && response.Headers.HasKeys())
            {
                Debug.WriteLine("Response Head .. {0} Received .. {1} .. {2} bytes", response.ResponseUri, response.ContentType, response.ContentLength);

                foreach (var item in response.Headers.AllKeys)
                {
                    // trace..
                    Debug.WriteLine("Response .. {0} {1}", item, response.Headers[item]);

                    if (item.Equals(HttpResponseHeader.AcceptRanges))
                    {
                        string isNone = response.Headers[item] as string;

                        if (!string.IsNullOrEmpty(isNone) && isNone.Equals("none"))
                        {
                            downloadRange = 0;
                            dataChunkCount = 1;
                        }

                        if (!UInt64.TryParse(isNone, out downloadRange))
                        {
                            downloadRange = 0;
                            dataChunkCount = 1;
                        }
                        else
                        {
                            returnCode = true;
                        }

                        dataChunkCount = 1;
                    }
                }
            }

            return returnCode;
        }

        internal bool createDataChunk()
        {
            throw new NotImplementedException();
        }

        internal bool getResumable()
        {
            throw new NotImplementedException();
        }

        public async void DownloadAsync()
        {
            // check if Range supported..
            if (getDownloadRange())
            {
                /* Create Data Chunks */
                //TODO create data chunks..
            }

            // check if Resume supported..
            /*if (getResumable())
            {
            }*/
        }
    }
}
