using System;

namespace MorganiteVerifier.Core
{
	public enum InstructionType : uint
	{
		NONE = 0,
		SELECT_AID = 1,
		READ_DATA = 2,
		WRITE_DATA = 3
	}
	
}
