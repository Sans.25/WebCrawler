﻿using System;
using System.Collections.Generic;

namespace MorganiteVerifier.Core
{
	public class MorganDataEventArgs
	{
        public MorganDataTemplate DataTemplate { get; protected set; }
		public Dictionary<int, MorganDataField> DataFieldDictionary { get; protected set; }

		public MorganDataEventArgs(MorganDataTemplate dataTemplate, ref Dictionary<int, MorganDataField> dataFieldDictionary)
		{
            if (dataTemplate == null)
            {
                throw new ArgumentNullException(nameof(dataTemplate));
            }

            if (dataFieldDictionary == null)
			{
				throw new ArgumentNullException(nameof(dataFieldDictionary));
			}

            this.DataTemplate = dataTemplate;
			this.DataFieldDictionary = dataFieldDictionary;
		}
	}
}
