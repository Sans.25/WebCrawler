﻿using System;

namespace MorganiteVerifier.Core
{
	public class InstructionSet : ICloneable
	{
		private byte[] raw = new byte[4];

		public byte CLA => raw[0];
		public byte INS => raw[1];
		public byte P1 => raw[2];
		public byte P2 => raw[3];

		public byte[] Sequence => raw;

		public InstructionSet(byte[] instructionSet)
		{
			if (instructionSet?.Length != 4)
			{
				throw new ArgumentException("Invalid Argument", nameof(instructionSet));
			}

			instructionSet.CopyTo(raw, 0);
		}

		public override string ToString()
		{
			return string.Format("[InstructionSet: CLA={0}, INS={1}, P1={2}, P2={3}]", CLA, INS, P1, P2);
		}

		public override bool Equals(object obj)
		{
			return obj is InstructionSet && this == (InstructionSet)obj;
		}

		public override int GetHashCode()
		{
			return BitConverter.ToInt32(raw, 0);
		}

        public object Clone()
        {
            return new InstructionSet(raw);
        }

        public static bool operator ==(InstructionSet setA, InstructionSet setB)
		{
			if (System.Object.ReferenceEquals(setA, setB))
			{
				return true;
			}

			if (System.Object.ReferenceEquals(setA, null) || System.Object.ReferenceEquals(setB, null))
			{
				return false;
			}

			return setA.CLA == setB.CLA && setA.INS == setB.INS && setA.P1 == setB.P1 && setA.P2 == setB.P2;
		}

		public static bool operator !=(InstructionSet setA, InstructionSet setB)
		{
			return !(setA == setB);
		}
	}
}
