﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using LibCSCReader;
using LibCSCReader.Common;
using ToolsLib;

using Log = System.Diagnostics.Debug;
using Initializer = MorganiteVerifier.Configuration;

namespace MorganiteVerifier.Core
{
    public class MorganData
    {
		protected ConvertibleObject data = null;
		protected MorganDataTemplate dataTemplate = null;

		public ConvertibleObject Data => data;
		public MorganDataTemplate DataTemplate => dataTemplate;

		protected MorganData(byte []data, bool SetLE = true)
        {
            this.data = ConvertibleObject.CreateAs(data, SetLE);
        }

        protected MorganData(ConvertibleObject data, MorganDataTemplate morganDataTemplate = null)
        {
            if (data == null)
				throw new ArgumentNullException(nameof(data));

            this.data = data;
			this.dataTemplate = morganDataTemplate;
        }

        public static MorganData Create(MorganDataTemplate morganDataTemplate)
        {
			//TODO setLE later..
            MorganData morganData = null;

            ConvertibleObject obj = ConvertibleObject.CreateAs(new byte[0]);

			DateTime currentTimestamp = DateTime.Now;
			//put current Date..
			DosDate date = new DosDate(currentTimestamp);
			DosTime time = new DosTime(currentTimestamp);


            var currentOffset = 0;
            foreach (var field in morganDataTemplate)
            {
                while (field.FieldOffset > currentOffset)
                {
                    // filler..
                    obj.Add(0x00);
					currentOffset++;
                }

                // TODO fill data not available
                obj.AddRange(new byte[field.FieldSize]);

				var offset = (currentOffset);
				ConvertibleObject fieldValue = field.FieldValue;
				if ((field.FieldValue?.Count ?? 0) == 0)
				{
					switch (field.CustimizationTypeAvailable)
					{
						case CustomizationType.DOSDATE:
							fieldValue = date.ToConvertibleObject();
							break;

						case CustomizationType.DOSTIME:
							fieldValue = time.ToConvertibleObject();
							break;

						default:
							Log.WriteLine("MorganData.Create .. value unavailable .. FieldName {0} FieldOffset {1}", field.FieldName, field.FieldOffset);
							break;
					}
				}

				if ((fieldValue?.Count ?? 0) != 0)
				for (int i = 0; i<fieldValue.Count; i++)
				{
						offset = (currentOffset + i);
						if (offset<obj.Count)
							obj[offset] = fieldValue[i];
				}

				currentOffset += field.FieldSize;
            }

            morganData = new MorganData(obj, morganDataTemplate);
            return morganData;
        }

		public bool Update(int dataFieldOffset, byte[] data, bool setLE = true)
		{
			//TODO check setLE
			try
			{
				if (data == null || data.Length == 0)
				{
					throw new ArgumentException("Invalid Argument", nameof(data));
				}
				else if (dataFieldOffset < 0)
				{
					throw new ArgumentException("Invalid Argument", nameof(dataFieldOffset));
				}

				if (dataTemplate != null)
				{
					int dataFieldI = 0;
/*					for (dataFieldI = 0; dataFieldI < dataTemplate.Count; dataFieldI++)
					{
						if (dataFieldOffset >= dataTemplate[dataFieldI].FieldOffset
						 && dataFieldOffset <= dataTemplate[dataFieldI].FieldOffset + dataTemplate[dataFieldI].FieldSize)
						{
							break;
						}
					}

					// not found.. ??
					if (dataFieldI == dataTemplate.Count)
					{
						throw new ArgumentException("Invalid Argument", nameof(dataFieldOffset));
					}*/

					int dataSize = data.Length;
					if (dataSize + dataFieldOffset > dataTemplate.MorganDataSize)
					{
						throw new ArgumentException("Invalid Input Data Size", nameof(data));
					}

					var dataField = dataTemplate[dataFieldI];
					while (dataSize > 0)
					{
						if (dataFieldOffset - dataField.FieldOffset < dataField.FieldSize)
						{
							if (dataField.FieldPermission.Write)
							{
								this.data[dataFieldOffset + data.Length - dataSize] = data[data.Length - dataSize];
							}
							else
							{
								Log.WriteLine("MorganData.Update .. No Permissions to Write at {0}", (dataFieldOffset + data.Length - dataSize));
							}
							dataSize--;
						}
						else
						{
							dataFieldI++;
							if (dataFieldI == dataTemplate.Count)
							{
								throw new ArgumentException("Invalid Input Data Size or Offset ..");
							}

							dataField = dataTemplate[dataFieldI];
						}
					}
				}
				else
				{
					throw new Exception("DataTemplate not found, can't update??");
				}
			}
			catch (Exception e)
			{
                Log.WriteLine("MorganData.Update .. {0} @ {1}", e.Message, e.StackTrace);

				return false;
			}

			return true;
		}

        public bool Customize(MorganDataField dataField, string inConvertibleTypeString, bool setLE = true)
        {
            try
            {
				// do not validate if dataField in dataTemplate

                if (dataField == null)
                    throw new ArgumentNullException(nameof(dataField), "Invalid DataField for Customization");

                else if (!dataField.CustomizableOnCreate && !Initializer.DeveloperMode)
                    throw new ArgumentException(nameof(dataField), "Selected DataField is not Customizable");

                var outData = ConvertibleObject.Create(inConvertibleTypeString, setLE);
                if (outData == null || outData.Count > dataField.FieldSize)
                {
                    outData = null;
                    throw new ArgumentOutOfRangeException(nameof(inConvertibleTypeString), "inData either not in correct format or Exceeds permissible Length " + dataField.FieldSize);
                }

                // Not Impossible Case Now ....??
                while (data.Count < dataField.FieldOffset + dataField.FieldSize)
                {
                    // TODO uint to int??
					data.Insert((int)dataField.FieldOffset, 0x00);
                }

                for (var offset = 0; offset < outData.Count; offset++)
                {
                    // TODO uint to int??
                    data[offset + (int)dataField.FieldOffset] = outData[offset];
                }

                Log.WriteLine("MorganData.Customize .. DataField at Offset {0} Customized with {1} Size of Data", dataField.FieldOffset, outData.Count);
            }
            catch (Exception e)
            {
                Log.WriteLine("MorganData.Customize .. {0} @ {1}", e.Message, e.StackTrace);
				return false;
            }

            return true;
        }

        public bool Customize(MorganDataField dataField, object inData, bool setLE = true)
        {
            try
            {
                if (dataField == null)
                    throw new ArgumentNullException(nameof(dataField), "Invalid DataField for Customization");

				else if (!dataField.CustomizableOnCreate && !Initializer.DeveloperMode)
                    throw new ArgumentException(nameof(dataField), "Selected DataField is not Customizable");

                ConvertibleObject outData = null;

                bool isValid = false;
                switch (dataField.CustimizationTypeAvailable)
                {
                    case CustomizationType.DOSDATE:
                        if (inData is DosDate)
                        {
                            DosDate date = (DosDate)inData;
                            outData = date.ToConvertibleObject(setLE);

                            isValid = true;
                        }
                        break;

                    case CustomizationType.DOSTIME:
                        if (inData is DosTime)
                        {
                            DosTime time = (DosTime)inData;
                            outData = time.ToConvertibleObject(setLE);

                            isValid = true;
                        }
                        break;

                    case CustomizationType.BYTE:
                        if (inData is byte)
                        {
                            outData = ConvertibleObject.CreateAs(new byte[0], setLE);
                            outData.Add((byte)inData);

                            isValid = true;
                        }
                        break;

                    case CustomizationType.INT16:
                        if (inData is Int16)
                        {
                            var dataBytes = BitConverter.GetBytes((Int16)inData);
                            outData = ConvertibleObject.CreateAs(dataBytes, BitConverter.IsLittleEndian == setLE);

                            isValid = true;
                        }
                        break;

                    case CustomizationType.UINT16:
                        if (inData is UInt16)
                        {
                            var dataBytes = BitConverter.GetBytes((UInt16)inData);
                            outData = ConvertibleObject.CreateAs(dataBytes, BitConverter.IsLittleEndian == setLE);

                            isValid = true;
                        }
                        break;

                    case CustomizationType.INT32:
                        if (inData is Int32)
                        {
                            var dataBytes = BitConverter.GetBytes((Int32)inData);
                            outData = ConvertibleObject.CreateAs(dataBytes, BitConverter.IsLittleEndian == setLE);

                            isValid = true;
                        }
                        break;

                    case CustomizationType.UINT32:
                        if (inData is UInt32)
                        {
                            var dataBytes = BitConverter.GetBytes((UInt32)inData);
                            outData = ConvertibleObject.CreateAs(dataBytes, BitConverter.IsLittleEndian == setLE);

                            isValid = true;
                        }
                        break;

                    case CustomizationType.CONVOBJ:
                        var inp = inData as ConvertibleObject;
                        if (inp != null)
                        {
                            outData = inp;

                            isValid = true;
                        }
                        break;


                    case CustomizationType.LIST:
                        if (inData is int)
                        {
                            outData = dataField.CustomizationList[(int)inData];

                            isValid = true;
                        }
                        break;

                    default:
                        throw new ArgumentException(nameof(dataField),  "Invalid Morgan DataField ..");
                        break;
                }

                if (!isValid || outData == null || outData.Count > dataField.FieldSize)
                {
                    outData = null;
                    throw new ArgumentOutOfRangeException(nameof(inData), "inData either not in correct format or Exceeds permissible Length " + dataField.FieldSize);
                }


                // Impossible Case ....??
                while (data.Count < dataField.FieldOffset + dataField.FieldSize)
                {
					data.Insert((int)dataField.FieldOffset, 0x00);
                }

                for (var offset = 0; offset < outData.Count; offset++)
                {
                    // TODO uint to int??
                    data[offset + (int)dataField.FieldOffset] = outData[offset];
                }

                Log.WriteLine("MorganData.Customize .. DataField at Offset {0} Customized with {1} Size of Data", dataField.FieldOffset, outData.Count);
            }
            catch (Exception e)
            {
                Log.WriteLine("MorganData.Customize .. {0} @ {1}", e.Message, e.StackTrace);
            }

            return true;
        }

        public static bool LoadFromFile(string fileName, out MorganData morganData, MorganDataTemplate morganDataTemplate = null, bool isAbsolute = false)
        {
            morganData = null;

            try
            {
                if (!isAbsolute)
                    fileName = Initializer.SavePath + "/" + fileName;

                var dataRead = File.ReadAllBytes(fileName);

                // verify size..
                if (morganDataTemplate == null)
                {
                    morganData = new MorganData(dataRead);
                }
                else if (morganDataTemplate != null && dataRead.Count() == morganDataTemplate.MorganDataSize)
                {
                    morganData = new MorganData(dataRead);
                    morganData.dataTemplate = morganDataTemplate;
                }
                else
                {
                    Log.WriteLine("MorganData.LoadFromFile .. DataRead Count {0} != DataSize {1}", dataRead.Count(), morganDataTemplate.MorganDataSize);
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                Log.WriteLine("MorganData.LoadFromFile {0} @ {1}", e.Message, e.StackTrace);
            }

            return false;
        }

        public bool SaveToFile(string fileName, bool isAbsolute = false)
        {
            try
            {
                if (!isAbsolute)
                    fileName = Initializer.SavePath + "/" + fileName;

                if (data != null 
                && (dataTemplate == null || (dataTemplate != null && data.Count == dataTemplate.MorganDataSize)))
                {
                    File.WriteAllBytes(fileName, data.RawConvertibleData);
                    Log.WriteLine("MorganData.SaveToFile .. [{0}] .. writeSize {1:D}", fileName, data.Count);
                }
                else
                {
                    Log.WriteLine("MorganData.SaveToFile .. No Data To Write");
                    //Log.WriteLine(LogPriority.Error, "Morganite", "MorganData.SaveToFile .. Data Count {0} != DataSize {1}", data?.Count() ?? 0, MorganDataField.MorganDataSize);
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                Log.WriteLine("MorganData.SaveToFile {0} @ {1}", e.Message, e.StackTrace);
            }

            return false;
        }

		public override string ToString()
		{
			IEnumerable<string> dataList = data?.Select(b => b.ToString("X2"));
			string str = ((dataList?.Count()??0) != 0) ? "<" + string.Join("> <", dataList) + ">" : "No Data";
			return string.Format("[MorganData: Data={0}, DataTemplate Size={1}]", str, dataTemplate?.MorganDataSize);
		}
   }
}