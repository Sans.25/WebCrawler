﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using LibCSCReader;

using ToolsLib;

namespace MorganiteVerifier.Core
{
    public class MorganDataField
    {
        public string FieldName { get; protected set; }

        public int FieldOffset { get; protected set; }

        public int FieldSize { get; protected set; }

        public string FieldDescription { get; protected set; }

        public MorganDataFieldPermission FieldPermission { get; protected set; }

        internal bool CustomizableOnCreate { get; set; } = false;

		internal CustomizationType CustimizationTypeAvailable { get; set; } = CustomizationType.CONVOBJ;

		internal List<ConvertibleObject> CustomizationList { get; set; } = null;

        public ConvertibleObject FieldValue { get; set; }

        protected MorganDataField(string fieldName, int fieldOffset, int fieldSize = 1, string fieldDescription = "", 
            MorganDataFieldPermission.Preset fieldPermission = MorganDataFieldPermission.Preset.ReadWrite, ConvertibleObject fieldValue = null)
        {
            FieldName = fieldName;
            FieldOffset = fieldOffset;
            FieldSize = fieldSize;
            FieldDescription = fieldDescription;
            FieldPermission = fieldPermission;
            FieldValue = fieldValue;

			CustimizationTypeAvailable = CustomizationType.CONVOBJ;
			CustomizationList = new List<ConvertibleObject>();
        }

		public static MorganDataField Create(MorganDataTemplate morganDataTemplate,
		                                     string fieldName, 
		                                     int fieldSize, 
		                                     string fieldDescription = "", 
		                                     MorganDataFieldPermission.Preset fieldPermission = MorganDataFieldPermission.Preset.ReadWrite,
		                                     ConvertibleObject fieldValue = null)
        {
			if (morganDataTemplate == null)
			{
				throw new ArgumentNullException(nameof(morganDataTemplate));
			}

			if (fieldSize < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(fieldSize));
			}

            MorganDataField dataField = new MorganDataField(fieldName, morganDataTemplate.MorganDataSize, fieldSize, fieldDescription, fieldPermission, fieldValue);
            
			morganDataTemplate.Add(dataField);

            return dataField;
        }

        public static MorganDataField CreateWithOffset(MorganDataTemplate morganDataTemplate,
                                                       int fieldOffset, 
		                                               string fieldName, 
		                                               int fieldSize, 
		                                               string fieldDescription,
		                                               MorganDataFieldPermission.Preset fieldPermission = MorganDataFieldPermission.Preset.ReadWrite,
		                                               ConvertibleObject fieldValue = null)
        {
			if (morganDataTemplate == null)
			{
				throw new ArgumentNullException(nameof(morganDataTemplate));
			}

			if (fieldOffset < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(fieldOffset));
			}

			if (fieldSize < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(fieldSize));
			}

			MorganDataField dataField = new MorganDataField(fieldName, fieldOffset, fieldSize, fieldDescription, fieldPermission, fieldValue);
            morganDataTemplate.Add(dataField);

            return dataField;
        }

		public void SetCustomization(bool customizationAvailable = true, CustomizationType custimizationTypeAvailable = CustomizationType.CONVOBJ, IEnumerable<ConvertibleObject> customizationList = null)
		{
			CustomizableOnCreate = customizationAvailable;
			CustimizationTypeAvailable = custimizationTypeAvailable;

			if (CustomizationList == null)
			{
				CustomizationList = new List<ConvertibleObject>();
			}

			if (customizationList != null)
			{
				CustomizationList.AddRange(customizationList);
			}
		}

        public override bool Equals(object obj)
        {
            return obj is MorganDataField && this == (MorganDataField) obj;
        }

        public static bool operator ==(MorganDataField morganDataField1, MorganDataField morganDataField2)
		{
			if (System.Object.ReferenceEquals(morganDataField1, morganDataField2))
				return true;

			if (System.Object.ReferenceEquals(morganDataField1, null) || System.Object.ReferenceEquals(morganDataField2, null))
				return false;

			bool nameMatch = morganDataField1.FieldName.Equals(morganDataField2.FieldName, StringComparison.Ordinal);
			bool offsetMatch = morganDataField1.FieldOffset == morganDataField2.FieldOffset;
			bool sizeMatch = morganDataField1.FieldSize == morganDataField2.FieldSize;
			bool descriptionMatch = morganDataField1.FieldDescription.Equals(morganDataField2.FieldName, StringComparison.Ordinal);
			bool permissionMatch = morganDataField1.FieldPermission.Value == morganDataField2.FieldPermission.Value;

			// TODO compare Store MorganDataTemplate later

			return nameMatch && offsetMatch && sizeMatch && descriptionMatch && permissionMatch;
		}

		public static bool operator !=(MorganDataField morganDataField1, MorganDataField morganDataField2)
		{
			return !(morganDataField1 == morganDataField2);
		}
    }
}