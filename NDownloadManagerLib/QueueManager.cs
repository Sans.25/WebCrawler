﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using NDownloadManagerLib.Base;
using NDownloadManagerLib.Base.Interfaces;

namespace NDownloadManagerLib
{
    /// <summary>
    /// Class QueueManager.
    /// </summary>
    /// <typeparam name="TRes">The type of the resource.</typeparam>
    /// <seealso cref="System.Collections.Generic.IEnumerable{Res}" />
    /// <seealso cref="System.Collections.IEnumerable" />
    /// <seealso cref="System.Collections.ICollection" />
    /// <seealso cref="System.IDisposable" />
    /// <seealso cref="System.Collections.Generic.IReadOnlyCollection{Res}" />
    public class QueueManager<TRes> : IEnumerable<TRes>, IEnumerable, ICollection, IDisposable, IReadOnlyCollection<TRes> where TRes : Resource
    {
        #region "OnSave Event"
        /// <summary>
        /// Delegate SaveEventHandler
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="SaveEventArgs"/> instance containing the event data.</param>
        internal delegate void SaveEventHandler(object sender, SaveEventArgs e);

        /// <summary>
        /// Class SaveEventArgs for use with OnSave Event
        /// </summary>
        /// <seealso cref="System.EventArgs" />
        internal class SaveEventArgs : EventArgs
        {
            /// <summary>
            /// Gets the resources list.
            /// </summary>
            /// <value>The resources.</value>
            internal List<TRes> Resources { get; private set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="SaveEventArgs"/> class.
            /// </summary>
            /// <param name="resources">The resources.</param>
            protected internal SaveEventArgs(List<TRes> resources)
            {
                Resources = resources;
            }
        }

        /// <summary>
        /// Occurs when [on save].
        /// Event Invoked On Save and before GC
        /// </summary>
        internal event SaveEventHandler OnSave;
        #endregion "OnSave Event"

        /// <summary>
        /// The _resource list
        /// All the Resources on which _requestList Requests perform read/write
        /// </summary>
        private List<TRes> _resourceList;

        /// <summary>
        /// The _request list
        /// In Requests imply all the Read/Write requests for resources
        /// Similar to Get / Set Requests or Enqueue/Dequeue
        /// </summary>
        private List<InRequest> _requestList;

        // TODO how to handle synchronization..

        /// <summary>
        /// Initializes a new instance of the <see cref="QueueManager{Res}"/> class.
        /// </summary>
        public QueueManager()
        {
            _requestList = new List<InRequest>();
            _resourceList = new List<TRes>();
        }

        /// <summary>
        /// IsDisposed?
        /// </summary>
        private bool _disposed = false;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _requestList?.Clear();

                OnSave?.Invoke(null, new SaveEventArgs(_resourceList));

                _resourceList?.Clear();
            }

            _disposed = true;
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
        public IEnumerator<TRes> GetEnumerator()
        {
            CheckValid();
            return _resourceList.GetEnumerator();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="QueueManager{Res}"/> class.
        /// </summary>
        ~QueueManager()
        {
            Dispose(false);
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins.</param>
        void ICollection.CopyTo(Array array, int index)
        {
            CheckValid();
            try
            {
                var src = _resourceList.ToArray();
                Array.Copy(src, 0, array, index, src.Length);
            }
            catch (ArgumentNullException)
            {
                throw new ArgumentNullException(nameof(array), "Arguement can not be null");
            }
            catch (RankException)
            {
                throw new RankException("Can not copy to multiDimensional array");
            }
            catch (ArrayTypeMismatchException)
            {
                throw new ArrayTypeMismatchException("Array type mismatch");
            }
            catch (InvalidCastException)
            {
                throw new InvalidCastException("Invalid type of array");
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentOutOfRangeException(nameof(array), "Array index out of range");
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Array index out of range", nameof(array));
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        /// <value>The count.</value>
        int ICollection.Count
        {
            get
            {
                CheckValid();
                return _resourceList.Count;
            }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        /// <value>The synchronize root.</value>
        /// <exception cref="System.NotSupportedException"></exception>
        public object SyncRoot
        {
            get
            {
                throw new NotSupportedException();
            }
        }

        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        /// <value><c>true</c> if this instance is synchronized; otherwise, <c>false</c>.</value>
        /// <exception cref="System.ObjectDisposedException">QueueManager;Object Disposed: QueueManager</exception>
        public bool IsSynchronized
        {
            get
            {
                CheckValid();
                return false;
            }
        }

        /// <summary>
        /// Checks the valid. In case disposed & internal Lists are valid
        /// </summary>
        /// <exception cref="System.ObjectDisposedException">QueueManager;Object Disposed: QueueManager</exception>
        private bool CheckValid()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException("QueueManager", "Object Disposed: QueueManager");
            }

            if (_resourceList != null && _requestList != null)
                return true;

            return false;
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        /// <value>The count.</value>
        int IReadOnlyCollection<TRes>.Count
        {
            get
            {
                CheckValid();
                return _resourceList.Count;
            }
        }
    }
}