﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NDownloadManagerLib.Base
{
    /// <summary>
    /// <b>FileTypeInfo class</b>
    /// FileTypeInformation about a FileType..
    /// </summary>
    /***
     * FileTypeInfo class
     */
    [DataContract]
    public class FileTypeInfo
    {
        [DataMember]
        public string Name { get; set; }
        public FileTypeInfo() { }
    }
}
