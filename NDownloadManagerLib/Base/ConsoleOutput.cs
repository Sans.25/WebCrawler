using System;

namespace NDownloadManagerLib.Base
{
    public class ConsoleOutput
    {
        /// <summary>
        /// Write to Console Output..
        /// </summary>
        /// <param name="str"></param>
        public static void Write(string str)
        {
            Console.Write(str);
        }

        /// <summary>
        /// Write to Console Output with New Line..
        /// </summary>
        /// <param name="str"></param>
        public static void WriteLine(string str)
        {
            Console.WriteLine(str);
        }

    };
}