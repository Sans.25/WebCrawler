﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NDownloadManagerLib.Base.Enums
{
    /// <summary>
    /// TransferFileSize enumerator
    /// </summary>
    public enum TransferFileSize : ulong
    {
        None = 0,

        Small = 512L,
        HalfByte = 512L,
        Kilobyte = 1024L,

        TenKilobyte = 10L * 1024L,

        Megabyte = 1024L * 1024L,
        Medium = 1024L * 1024L,

        TenMegabyte = 10L * 1024L * 1024L,

        HundredMegabyte = 100L * 1024L * 1024L,

        Gigabyte = 1024L * 1024L * 1024L,

        TerraByte = 1024L * 1024L * 1024L * 1024L,

        Unknown = 1
    }
}
