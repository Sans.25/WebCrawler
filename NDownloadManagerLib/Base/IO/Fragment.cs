﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace NDownloadManagerLib.Base.IO
{
    public class Fragment : Comparer<Fragment>, IComparable<Fragment>
    {
        /// <summary>
        /// File to which the Fragment belongs..
        /// </summary>
        public NDownloadManagerLib.Base.IO.File File { get; set; }

        /// <summary>
        /// IsFile property
        /// This implies the Fragment has a temporary file linked to it to which all the data is written.
        /// </summary>
        public bool IsFile { get; internal set; } = false;

        /// <summary>
        /// IsBuffered property
        /// This implies the Fragment is in the MemoryStream or Memory Buffer as well as in the File..
        /// This is suitable for small sized <see cref="Fragment"/>s..
        /// </summary>
        public bool IsBuffered { get; internal set; } = false;

        public MemoryStream Buffer { get; internal set; } = new MemoryStream();

        /// <summary>
        /// Filename property
        /// Temporary filename of the Fragment..
        /// This implies the fragment starts at 0th location of the file but, the 0th byte corresponds to the <see cref="StartByte"/> of the Fragment in the <see cref="File"/> of which this is fragment of and is uptill <see cref="EndByte"/>.
        /// </summary>
        public string Filename { get; internal set; } = null;

        private readonly object _lockPosition = new object();
        private long _startByte = 0;
        private long _endByte = -1;

        /// <summary>
        /// StartByte property
        /// The starting byte index of the Fragment in the <see cref="File"/> of which this fragment exists.
        /// </summary>
        public long StartByte 
        { 
            get 
            {
                long returnValue = 0;
                lock (_lockPosition)
                {
                    returnValue = _startByte;
                }

                return returnValue;
            }
            set 
            {
                lock (_lockPosition)
                {
                    _startByte = value;
                }
            } 
        }

        /// <summary>
        /// EndByte property
        /// The ending byte index of the Fragment in the <see cref="File"/> of which this fragment exists.
        /// </summary>
        public long EndByte 
        {
            get
            {
                long returnValue = 0;
                lock (_lockPosition)
                {
                    returnValue = _endByte;
                }

                return returnValue;
            }

            set
            {
                lock (_lockPosition)
                {
                    _endByte = value;
                }
            }
        }

        private readonly object _lockIsActive = new object();

        private string _error = string.Empty;
        private bool _isActive = false;

        public String Error
        {
            get
            {
                string stringError = "";
                lock (_lockIsActive) { stringError = _error; }

                return stringError;
            }

            internal set
            { 
                lock (_lockIsActive) { _error = value; }
            }
        }

        public bool IsActive 
        { 
            get 
            {
                bool returnValue = false;
                lock (_lockIsActive) { returnValue = _isActive; }

                return returnValue;
            }
            internal set 
            {
                lock (_lockIsActive) { _isActive = value; }
            } 
        }

        internal MemoryStream buffer = null;
        internal FileStream fileStream = null;

        public Fragment(File _file) 
        {
            this.File = _file;
        }

        public Fragment(string filename, File _file)
        {
            IsFile = true;
            Filename = filename;
            IsBuffered = false;

            this.File = _file;
        }

        /// <summary>
        /// Fragment constructor method
        /// </summary>
        /// <param name="generateTemporaryFilename">Specifies if the Fragment is saved in a Temporary File</param>
        /// <param name="bufferFragment">Specifies if the Fragment is a MemoryStream Buffer</param>
        /// <param name="bufferSize">Specifies the capacity of the MemoryStream Buffer</param>
        public Fragment(bool generateTemporaryFilename = false, bool bufferFragment = false, int bufferSize = 0)
        {
            if (generateTemporaryFilename)
            {
                Filename = Path.GetTempFileName();
                IsFile = true;
                IsBuffered = false;
            }

            if (bufferFragment || bufferSize > 0)
            {
                bufferFragment = true;

                if (bufferSize > 0)
                    buffer = new MemoryStream(bufferSize);
                else
                    bufferFragment = false;
            }

        }

        /// <summary>
        /// Download Method
        /// This method downloads the fragment from the internet to the local pc.
        /// </summary>
        public void Download()
        {
            // for downloading we will need settings or configuration for the httpWebRequest,
            // like the method used to get the download,
            // referrer if any and cookies if any..
            if (!string.IsNullOrEmpty(this.File.Url))
            {
                HttpWebRequest httpWebRequest = HttpWebRequest.CreateHttp(this.File.Url);
                httpWebRequest.Method = "GET";

                // EndByte is unknown..
                // Find EndByte..
                if (EndByte == -1)
                {
                    // Get content length..
                    var webRequest = HttpWebRequest.Create(this.File.Url);
                    webRequest.Method = "HEAD";
                    var webResponse = webRequest.GetResponse();
                    long contentLength = 0;
                    if (long.TryParse(webResponse.Headers.Get("Content-Length"), out contentLength))
                    {
                        // Set EndByte..
                        EndByte = contentLength;
                    }
                    webResponse.Close();
                }

                // It is possible that EndByte still maybe -1..
                if (EndByte != -1) 
                {
                    // Set Range to download..
                    // Set ContentRange to download..
                    var contentRange = (new ContentRangeHeaderValue(StartByte, EndByte)).ToString();
                    var range = (new RangeHeaderValue(StartByte - 1, EndByte)).ToString();
                    Debug.WriteLine("Range={0}", range);
                    Debug.WriteLine("Content-Range={0}", contentRange);

                    // bytes are numbered from 0.. in Range..
                    httpWebRequest.AddRange(StartByte - 1, EndByte);

                    //httpWebRequest.Headers.Add(HttpRequestHeader.Range, range);
                    httpWebRequest.Headers.Add(HttpRequestHeader.ContentRange, contentRange);
                }

                if (string.IsNullOrEmpty(this.File.Filename))
                {
                    Uri uri = new Uri(this.File.Url);
                    if (uri != null)
                    {
                        this.File.Filename = this.Filename = Path.GetFileName(uri.LocalPath);
                    }
                }

                httpWebRequest.BeginGetResponse(DownloadCallbackAsync, httpWebRequest);
                // TODO complete this
            }
        }

        /// <summary>
        /// DownloadCallbackAsync method
        /// Downloads data in a thread run from <see cref="HttpWebRequest.BeginGetRequestStream(AsyncCallback, object)">BeginGetResponse"/> of <see cref="HttpWebRequest"/>..
        /// </summary>
        /// <param name="asyncResult">WebRequest is passed in asyncResult parameter..</param>
        internal void DownloadCallbackAsync(IAsyncResult asyncResult)
        {
            FileStream fs = null;
            try
            {
                IsActive = true;
                Error = "";
                HttpWebRequest httpWebRequest = asyncResult.AsyncState as HttpWebRequest;

                using (HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.EndGetResponse(asyncResult))
                {
                    if (System.IO.File.Exists(this.Filename))
                    {
                        fs = new FileStream(this.Filename, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                    }
                    else
                    {
                        fs = new FileStream(this.Filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
                    }

                    byte[] buffer = new byte[4096];
                    long totalSize = 0L;
                    long seek = StartByte;
                    //TODO file is empty with 0 bytes size, how to seek.. 
                    //NOTE Seek can go beyond length of file..
                    //StartByte starts at 1..
                    // Not using IsBuffered or Buffer..
                    /*if (IsBuffered)
                    {
                        Buffer.Seek(0, SeekOrigin.Begin);
                    }*/
                    fs.Seek(seek - 1, SeekOrigin.Begin);
                    using (Stream inputStream = httpWebResponse.GetResponseStream())
                    {
                        int readSize = 0;
                        do
                        {
                            readSize = inputStream.Read(buffer, 0, buffer.Length);

                            if (readSize > 0)
                            {
                                fs.Write(buffer, 0, readSize);
                                /*if (IsBuffered)
                                    Buffer.Write(buffer, 0, readSize);*/

                                StartByte += readSize;
                            }
                        } while (readSize > 0 || (EndByte != -1 && EndByte < StartByte));
                    }
                    fs.Flush();
                    fs.Close();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                Error = e.ToString();

                try
                {
                    if (fs != null) { fs.Flush(); fs.Close(); }
                }
                catch
                {
                    Debug.WriteLine(e.ToString());
                }
            }

            IsActive = false;
        }

        /// <summary>
        /// Compare method
        /// </summary>
        /// <param name="x">First <see cref="Fragment"/> for Comparison</param>
        /// <param name="y">Second <see cref="Fragment"/> for Comparison</param>
        /// <returns>CompareTo values based upon <see cref="StartByte"/> property.</returns>
        public override int Compare(Fragment x, Fragment y)
        {
            return x.StartByte.CompareTo(y.StartByte);
        }

        /// <summary>
        /// CompareTo method
        /// </summary>
        /// <param name="other">The other <see cref="Fragment"/> for comparison</param>
        /// <returns><see cref="long.CompareTo"/> values based upon <see cref="StartByte"/> property.</returns>
        public int CompareTo(Fragment other)
        {
            return StartByte.CompareTo(other?.StartByte);
        }
    }
}
