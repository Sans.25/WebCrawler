﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NDownloadManagerLib.Base.Net
{
    [DataContract]
    public class Proxy
    {
        [DataMember]
        public string Name { get; set; }

        private readonly object _lock = new object();

        private string _address = "";
        private int _port = 80;

        [DataMember]
        public string Address 
        { 
            get
            {
                string returnValue = "";
                lock (_lock)
                {
                    returnValue = _address;
                }

                return returnValue;
            }
            set
            {
                lock (_lock)
                {
                    _address = value;
                }
        }

        [DataMember]
        public int Port 
        { 
            get
            {
                int returnValue = 80;
                lock (_lock)
                {
                    returnValue = _port;
                }
                return returnValue;
            }
            set
            {
                lock (_lock)
                {
                    _port = value;
                }
            }
        }

        public Proxy(string address, int port) 
        { 
            Name = Address = address; 
            Port = port; 
        }

        public void AssignProxy(ref HttpWebRequest httpWebRequest)
        {
            var proxy = new WebProxy(Address, Port);

            httpWebRequest.Proxy = proxy;
        }
    }
}
