﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace NDownloadManagerLib.Base.Net
{
    [Serializable]
    //[DataContract]
    public class UrlConfiguration : ISerializable
    {
        /// <summary>
        /// Url property
        /// </summary>
        //[DataMember]
        public string Url { get; set; }

        /// <summary>
        /// Domain property
        /// </summary>
        //[DataMember]
        public string Domain { get; set; }

        /// <summary>
        /// Name property
        /// </summary>
        //[DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Description property
        /// </summary>
        //[DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Cookies property
        /// </summary>
        //[DataMember]
        public CookieContainer Cookies { get; set; } = null;

        /// <summary>
        /// Referer property
        /// </summary>
        //[DataMember]
        public string Referer { get; set; }

        /// <summary>
        /// Method property
        /// </summary>
        //[DataMember]
        public string Method { get; set; } = "GET";

        /*
        /// <summary>
        /// Encoding property
        /// </summary>
        [DataMember]
        public Encoding Encoding { get; set; } = Encoding.UTF8;
        */

        /// <summary>
        /// Useragent Property
        /// </summary>
        //[DataMember]
        public string Useragent { get; set; } = string.Empty;

        /// <summary>
        /// Username property
        /// </summary>
        //[DataMember]
        public string Username { get; set; } = string.Empty;

        /// <summary>
        /// Password property
        /// </summary>
        //[DataMember]
        public string Password { get; set; } = string.Empty;

        /// <summary>
        /// IsBasicAuthentication property
        /// </summary>
        //[DataMember]
        public bool IsBasicAuthentication { get; set; } = false;

        /// <summary>
        /// Configure method
        /// </summary>
        /// <param name="httpWebRequest"><see cref="HttpWebRequest"/> to be configured..</param>
        public void Configure(ref HttpWebRequest httpWebRequest)
        {
            // Useragent
            if (!string.IsNullOrEmpty(Useragent))
                httpWebRequest.UserAgent = Useragent;
            else
                httpWebRequest.UserAgent = @"NDownloadManager v1.0";

            // CookieContainer
            if (httpWebRequest.CookieContainer == null)
            {
                if (this.Cookies == null)
                    httpWebRequest.CookieContainer = new CookieContainer();
                else
                    httpWebRequest.CookieContainer = this.Cookies;
            }
            else if (this.Cookies != null)
            {
                Uri uri = new Uri(this.Url);
                var cookieCollection = this.Cookies.GetCookies(uri);

                foreach (Cookie cookie in cookieCollection)
                {
                    httpWebRequest.CookieContainer.Add(cookie);
                }
            }

            // Method
            if (this.Method == "GET" || this.Method == "POST" || this.Method == "PUT" || this.Method == "HEAD")
                httpWebRequest.Method = this.Method;

            //httpWebRequest.Credentials
            if (!string.IsNullOrEmpty(this.Username))
            {
                NetworkCredential credential = new NetworkCredential(this.Username, this.Password);
                httpWebRequest.Credentials = credential;
            }
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Url", Url);
            info.AddValue("Domain", Domain);
            info.AddValue("Name", Name);
            info.AddValue("Description", Description);
            info.AddValue("Cookies", Cookies);
            info.AddValue("Referer", Referer);
            info.AddValue("Method", Method);
            //info.AddValue("Encoding", this.Encoding);
            info.AddValue("Useragent", Useragent);
            info.AddValue("Username", Username);
            info.AddValue("Password", Password);
            info.AddValue("IsBasicAuthentication", IsBasicAuthentication);
        }

        public UrlConfiguration()
        {
        }

        public UrlConfiguration(SerializationInfo info, StreamingContext context)
        {
            Url = (string)info.GetValue("Url", typeof(string));
            Domain = (string)info.GetValue("Domain", typeof(string));
            Name = (string)info.GetValue("Name", typeof(string));
            Description = (string)info.GetValue("Description", typeof(string));
            Cookies = (CookieContainer)info.GetValue("Cookies", typeof(CookieContainer));
            Referer = (string)info.GetValue("Referer", typeof(string));
            Method = (string)info.GetValue("Method", typeof(string));
            //this.Encoding = (Encoding)info.GetValue("Encoding", typeof(Encoding));
            Useragent = (string)info.GetValue("Useragent", typeof(string));
            Username = (string)info.GetValue("Username", typeof(string));
            Password = (string)info.GetValue("Password", typeof(string));
            IsBasicAuthentication = (bool)info.GetValue("IsBasicAuthentication", typeof(bool));
        }

        public bool Equals(UrlConfiguration other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            if (this.Url.Equals(other.Url)) return true;
            return false;
        }
    }
}
