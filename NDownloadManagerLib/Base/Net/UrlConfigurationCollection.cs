﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NDownloadManagerLib.Base.Net
{
    [Serializable]
    public class UrlConfigurationCollection : BlockingCollection<UrlConfiguration>, ISerializable
    {
        private bool _lockTaken = false;
        private SpinLock _spinLock = new SpinLock();

        public bool Configure(ref HttpWebRequest httpWebRequest, bool configureAll = false)
        {
            if (httpWebRequest == null) { throw new ArgumentNullException(nameof(httpWebRequest)); }

            var urlConfiguration = Find(httpWebRequest.RequestUri.ToString(), ref httpWebRequest, configureAll);
            if (urlConfiguration == null) { return false; }

            if (!configureAll)
                urlConfiguration.Configure(ref httpWebRequest);

            return true;
        }

        internal UrlConfiguration Find(string url, ref HttpWebRequest httpWebRequest, bool configureAll = false)
        {
            // this must be thread synchronized..

            int maxLength = 0;
            UrlConfiguration returnValue = null;

            _spinLock.Enter(ref _lockTaken);

            foreach (var urlConfiguration in this)
            {
                if (url.StartsWith(urlConfiguration.Url))
                {
                    if (urlConfiguration.Url.Length > maxLength)
                    {
                        maxLength = urlConfiguration.Url.Length;
                        returnValue = urlConfiguration;
                    }

                    if (configureAll)
                    {
                        urlConfiguration.Configure(ref httpWebRequest);
                    }
                }
            }

            _spinLock.Exit();

            return returnValue;
        }

        public UrlConfigurationCollection() { }

        public UrlConfigurationCollection(IProducerConsumerCollection<UrlConfiguration> urlConfigurations) : base(urlConfigurations)
        {
        }

        public UrlConfigurationCollection(SerializationInfo info, StreamingContext context)
        {
            _urlConfigurations = (UrlConfiguration[])info.GetValue("UrlConfigurations", typeof(UrlConfiguration[]));
        }

        private UrlConfiguration[] _urlConfigurations = null;

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            if (_urlConfigurations != null)
            {
                foreach (var urlConfiguration in _urlConfigurations)
                {
                    this.Add(urlConfiguration);
                }

                _urlConfigurations = null;
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            List<UrlConfiguration> urlConfigurations = new List<UrlConfiguration>();
            int i = 0;

            _spinLock.Enter(ref _lockTaken);
            while (this.Count > 0)
            {
                var item = this.Take();
                urlConfigurations.Add(item);
            }

            info.AddValue("UrlConfigurations", urlConfigurations);

            foreach (var item in urlConfigurations)
                this.Add(item);

            _spinLock.Exit();
        }

    }
}
