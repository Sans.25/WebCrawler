﻿using System;
using NDownloadManagerLib.Base.Enums;

namespace NDownloadManagerLib.Base.Interfaces
{
    /// <summary>
    /// Class Resource.
    /// Implements the <see cref="NDownloadManagerLib.Base.Interfaces.Request" />
    /// </summary>
    /// <seealso cref="NDownloadManagerLib.Base.Interfaces.Request" />
    public abstract class Resource : Request, IResource
    {
        /// <summary>
        /// The internal lock for Synchronization
        /// </summary>
        protected readonly object _internalLock;

        /// <summary>
        /// Gets the synchronize lock.
        /// </summary>
        /// <value>The synchronize lock.</value>
        public object SyncLock => _internalLock;

        /// <summary>
        /// The locked lock for Synchronization
        /// </summary>
        private readonly object _lockedLock;

        /// <summary>
        /// The _is locked
        /// </summary>
        protected bool _isLocked;

        /// <summary>
        /// The state changed
        /// </summary>
        protected internal bool _stateChanged = false;

        protected static readonly object _resourcesLock = new object();
        protected static ulong _resources = 0;

        #region "Lock Events"
        /// <summary>
        /// Delegate LockEventHandler
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="LockEventArgs"/> instance containing the event data.</param>
        protected internal delegate void LockEventHandler(object sender, LockEventArgs e);

        /// <summary>
        /// Class LockEventArgs.
        /// </summary>
        /// <seealso cref="System.EventArgs" />
        public class LockEventArgs : EventArgs
        {
            /// <summary>
            /// Gets or sets the state.
            /// </summary>
            /// <value>The state.</value>
            public LockState State { get; protected set; }

            /// <summary>
            /// Gets or sets the lock resource.
            /// </summary>
            /// <value>The lock resource.</value>
            public Resource LockResource { get; protected set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="LockEventArgs"/> class.
            /// </summary>
            /// <param name="resource">The resource.</param>
            /// <param name="state">The state.</param>
            protected internal LockEventArgs(Resource resource, LockState state)
            {
                LockResource = resource;
                State = state;
            }
        }

        /// <summary>
        /// Occurs when [lock state change].
        /// </summary>
        protected internal event LockEventHandler LockStateChange;
        #endregion "Lock Events"

        /// <summary>
        /// Gets or sets a value indicating whether this instance is locked.
        /// </summary>
        /// <value><c>true</c> if this instance is locked; otherwise, <c>false</c>.</value>
        public bool IsLocked
        {
            get
            {
                lock (_lockedLock)
                {
                    return _isLocked;
                }
            }
            protected set
            {
                lock (_lockedLock)
                {
                    if (_isLocked != value)
                        _stateChanged = true;

                    _isLocked = value;
                }
            }
        }

        protected ulong _resourceId = 0;
        public ulong ResourceId
        {
            get
            {
                ulong returnValue = 0;
                lock (_internalLock)
                {
                    returnValue = _resourceId;
                }

                return returnValue;
            }
            // this must be protected..
            /*protected */
            set
            {
                lock (_internalLock)
                {
                    _resourceId = value;
                }
            }
        }
        public bool IsAvailable { get; set; }
        public string Name { get; set; }
        public string Target { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Resource" /> class.
        /// </summary>
        /// <param name="project">The project associated with this resource</param>
        protected Resource(ulong project = InvalidProjectNum) : base(RequestMode.Resource, InvalidResourceId, project)
        {
            lock (_resourcesLock)
            {
                ResourceId = ++_resources;
            }
        }

        /// <summary>
        /// Locks this instance.
        /// </summary>
        protected internal void Lock()
        {
            IsLocked = true;
            if (_stateChanged)
                OnLockStateChange();
        }

        /// <summary>
        /// Unlock.
        /// </summary>
        protected internal void UnLock()
        {
            IsLocked = false;
            if (_stateChanged)
                OnLockStateChange();
        }

        /// <summary>
        /// Called when LockStateChange.
        /// </summary>
        protected virtual void OnLockStateChange()
        {
            LockStateChange?.Invoke(this, new LockEventArgs(this, LockState.UnLocked));
        }

        public virtual bool GetResource()
        {
            throw new NotImplementedException();
        }

        public virtual bool SendResource()
        {
            throw new NotImplementedException();
        }
    }
}