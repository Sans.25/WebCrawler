﻿using System;
using NDownloadManagerLib;
using NDownloadManagerLib.Base.Enums;
using NDownloadManagerLib.Base.Interfaces;

namespace NDownloadManagerLib.Base
{
    /// <summary>
    /// Class OutRequest.
    /// </summary>
    /// <seealso cref="NDownloadManagerLib.Base.Interfaces.Request" />
    /// <seealso cref="Request" />
    public class OutRequest<TRes> : InRequest where TRes : Resource
    {
        /// <summary>
        /// Gets or sets the out resource.
        /// </summary>
        /// <value>The out resource.</value>
        public TRes OutResource { get; protected set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="OutRequest{TRes}" /> class.
        /// </summary>
        /// <param name="requestType">Type of Request being Requested</param>
        /// <param name="outResource">The out resource.</param>
        /// <param name="projectNum">Project associated with the request</param>
        /// <param name="globalResourceRequest">if set to <c>true</c> [global resource request].</param>
        /// <exception cref="ArgumentException">InRequest can not be used for reading resources</exception>
        protected internal OutRequest(RequestMode requestType, TRes outResource, ulong projectNum = InvalidProjectNum, bool globalResourceRequest = true) : base(requestType, InvalidResourceId, projectNum, globalResourceRequest)
        {
            if ((requestType & RequestMode.ReadRequest) == RequestMode.ReadRequest)
            {
                throw new ArgumentException("OutRequest can not be used for reading resources", nameof(requestType));
            }

            OutResource = outResource;
        }
    }
}