﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using NDownloadManagerLib.Base.Enums;
using NDownloadManagerLib.Base.Interfaces;

namespace NDownloadManagerLib.Base
{
    /// <summary>
    /// Class RequestQueue.
    /// </summary>
    /// <typeparam name="TRes">The type of the resource.</typeparam>
    /// <seealso cref="System.Collections.Generic.IEnumerable{Request}" />
    /// <seealso cref="System.Collections.Generic.IReadOnlyCollection{Request}" />
    /// <seealso cref="System.Collections.Generic.IEnumerable{Request}" />
    /// <seealso cref="System.Collections.IEnumerable" />
    /// <seealso cref="System.IDisposable" />
    /// <seealso cref="System.Collections.ICollection" />
    /// <seealso cref="System.Collections.Generic.IReadOnlyCollection{Request}" />
    [ComVisible(false)]
    public class RequestQueue<TRes> : IEnumerable<Request>, IEnumerable, IDisposable, ICollection, IReadOnlyCollection<Request> where TRes : Resource
    {
        /// <summary>
        /// Internal requestNum counter for <see cref="GenerateRequestNum"/>
        /// </summary>
        private ulong _requestNum;

        /// <summary>
        /// The Current Request Number
        /// </summary>
        private ulong _currentRequestNum;

        /// <summary>
        /// The InRequest semaphore
        /// </summary>
        private SemaphoreSlim _inRequestSemaphore;

        /// <summary>
        /// The OutRequest semaphore
        /// </summary>
        private SemaphoreSlim _outRequestSemaphore;

        /// <summary>
        /// Request List
        /// </summary>
        private List<Request> _requestList;

        /// <summary>
        /// The _disposed
        /// </summary>
        private bool _disposed = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestQueue{TRes}" /> class.
        /// </summary>
        /// <param name="capacity">The capacity.</param>
        /// <exception cref="System.ArgumentException">Invalid capacity</exception>
        public RequestQueue(int capacity = -1)
        {
            if (capacity < 1 && capacity != -1)
            {
                throw new ArgumentException("Invalid capacity", nameof(capacity));
            }

            if (capacity == -1)
            {
                _requestList = new List<Request>();
                _inRequestSemaphore = new SemaphoreSlim(1);
            }
            else
            {
                _requestList = new List<Request>(capacity);
                _inRequestSemaphore = new SemaphoreSlim(1, capacity);
            }
            _outRequestSemaphore = new SemaphoreSlim(1);
        }

        /// <summary>
        /// Generates the request number for internal purposes.
        /// Internally used on <see cref="Create"/> and <see cref="CreateWithResource"/>
        /// </summary>
        /// <returns>System.UInt64.</returns>
        protected ulong GenerateRequestNum()
        {
            if (_requestNum + 1 == 0) // overflow
            {
                // avoid InvalidRequestNum from getting assigned..
                _requestNum = 0;
            }

            return ++_requestNum;
        }

        /// <summary>
        /// Creates the specified requested resource identifier.
        /// Internally creates InRequest.
        /// This does not add generated Request to Queue.
        /// </summary>
        /// <param name="requestedResourceId">The requested resource identifier.</param>
        /// <param name="projectNum">The project number.</param>
        /// <param name="globalResourceRequest">if set to <c>true</c> [global resource request].</param>
        /// <returns>Creates Request for Resources</returns>
        /// <seealso cref="InRequest"/>
        public Request Create(ulong requestedResourceId = Request.InvalidResourceId, ulong projectNum = Request.InvalidProjectNum, bool globalResourceRequest = true)
        {
            RequestMode requestFlags = RequestMode.ReadRequest;
            if (projectNum != Request.InvalidProjectNum)
                requestFlags |= RequestMode.Project;

            if (globalResourceRequest)
                requestFlags |= RequestMode.Global;

            var inRequest = new InRequest(requestFlags, requestedResourceId, projectNum, globalResourceRequest)
            {
                Parent = this
            };
            return inRequest;
        }

        /// <summary>
        /// Creates the request with resource.
        /// Internally Creates OutRequest.
        /// This does not add generated Request to Queue.
        /// </summary>
        /// <param name="resource">The resource.</param>
        /// <param name="projectNum">The project number.</param>
        /// <param name="globalResourceRequest">if set to <c>true</c> [global resource request].</param>
        /// <returns>Creates Request with Resources</returns>
        /// <seealso cref="OutRequest{TRes}"/>
        public Request CreateWithResource(TRes resource, ulong projectNum = Request.InvalidProjectNum,
            bool globalResourceRequest = true)
        {
            RequestMode requestFlags = RequestMode.WriteRequest;
            if (projectNum != Request.InvalidProjectNum)
                requestFlags |= RequestMode.Project;

            if (globalResourceRequest)
                requestFlags |= RequestMode.Global;

            var outRequest = new OutRequest<TRes>(requestFlags, resource, projectNum, globalResourceRequest)
            {
                Parent = this
            };
            return outRequest;
        }

        #region "Enqueue"
        /// <summary>
        /// Enqueues the specified resource.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns><c>true</c> if successful, <c>false</c> otherwise.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.ArgumentException">Invalid request, Parent mismatch</exception>
        public bool Enqueue(Request request)
        {
            try
            {
                _outRequestSemaphore.Wait();
                InternalEnqueue(request);
                _outRequestSemaphore.Release();
                return true;
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }

            // ReSharper disable once HeuristicUnreachableCode
            return false;
        }

        /// <summary>
        /// Enqueues the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="timeout">The timeout. Set to <c>-1</c> for infinite wait.</param>
        /// <returns><c>true</c> if successful, <c>false</c> otherwise.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.ArgumentException">Invalid request, Parent mismatch</exception>
        public bool Enqueue(Request request, TimeSpan timeout)
        {
            try
            {
                if (!_outRequestSemaphore.Wait(timeout))
                {
                    return false;
                }
                InternalEnqueue(request);
                _outRequestSemaphore.Release();
                return true;
            }
            catch (ObjectDisposedException)
            {
                throw new ObjectDisposedException(nameof(timeout));
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentOutOfRangeException(nameof(timeout));
            }

            // ReSharper disable once HeuristicUnreachableCode
            return false;
        }


        /// <summary>
        /// Enqueues the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns><c>true</c> if successful, <c>false</c> otherwise.</returns>
        /// <exception cref="System.OperationCanceledException"></exception>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.ArgumentException">Invalid request, Parent mismatch</exception>
        public bool Enqueue(Request request, CancellationToken cancellationToken)
        {
            try
            {
                _outRequestSemaphore.Wait(cancellationToken);
                InternalEnqueue(request);
                _outRequestSemaphore.Release();
                return true;
            }
            catch (OperationCanceledException)
            {
                throw new OperationCanceledException(cancellationToken);
            }
            catch (ObjectDisposedException)
            {
                throw new ObjectDisposedException(nameof(cancellationToken));
            }

            return false;
        }


        /// <summary>
        /// Enqueues the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="timeout">The timeout.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns><c>true</c> if successful, <c>false</c> otherwise.</returns>
        /// <exception cref="System.OperationCanceledException"></exception>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.ArgumentException">Invalid request, Parent mismatch</exception>
        public bool Enqueue(Request request, TimeSpan timeout, CancellationToken cancellationToken)
        {
            try
            {
                if (!_outRequestSemaphore.Wait(timeout, cancellationToken))
                {
                    return false;
                }
                InternalEnqueue(request);
                _outRequestSemaphore.Release();
                return true;
            }
            catch (OperationCanceledException)
            {
                throw new OperationCanceledException(cancellationToken);
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentOutOfRangeException(nameof(timeout));
            }
            
            // ReSharper disable once HeuristicUnreachableCode
            return false;
        }
        #endregion "Enqueue"

        #region "Internal Enqueue Dequeue"
        /// <summary>
        /// Internals the enqueue.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.ArgumentException">Invalid request, Parent mismatch</exception>
        private void InternalEnqueue(Request request)
        {
            CheckValid();
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            if (request.Parent != this)
            {
                throw new ArgumentException("Invalid request, Parent mismatch", nameof(request));
            }

            request.RequestNum = GenerateRequestNum();
            request.Completed += (sender, args) =>
            {
                var requestQueue = (sender as Request)?.Parent as RequestQueue<TRes>;
                requestQueue?._requestList?.Remove((Request)sender);
            };
            _requestList.Add(request);
        }

        /// <summary>
        /// Internals the Dequeue.
        /// </summary>
        /// <returns>Request.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        private Request InternalDequeue()
        {
            CheckValid();

            Retry:
            var returnRequest = _requestList.FirstOrDefault(request => request?.RequestNum >= _currentRequestNum);

            // in case of _currentRequestNum == ulong.MaxValue above won't find any element.
            if (returnRequest == null && _requestList.Count > 0)
            {
                _currentRequestNum = 1;
                goto Retry;
            }

            if (returnRequest != null)
            {
                _currentRequestNum = returnRequest.RequestNum + 1;
                if (_currentRequestNum == 0)
                {
                    _currentRequestNum ++;
                }

                _requestList.Remove(returnRequest);
            }

            return returnRequest;
        }

        /// <summary>
        /// Internals the Dequeue.
        /// </summary>
        /// <returns>Request.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        private Request InternalPriorityDequeue()
        {
            CheckValid();

            if (_requestList.Count == 0)
                return null;

            var topPriority = _requestList.Min(request => request.RequestPriority);

            // consider each item with priority 0.
            // first item removed from list
            // space created filled using new item
            // hence, again new item gets retrieved
            // older stays in the list.
            // eventually all get called.
            var returnRequest = _requestList.FirstOrDefault(request => request.RequestPriority >= topPriority);

            if (returnRequest != null)
            {
                // can't calculate. its priority based
                //_currentRequestNum = returnRequest.RequestNum + 1;
                //if (_currentRequestNum == 0)
                //{
                //    _currentRequestNum++;
                //}

                _requestList.Remove(returnRequest);
            }

            return returnRequest;
        }
        #endregion "Internal Enqueue Dequeue"

        /// <summary>
        /// Checks the validity of the instance.
        /// Validates if disposed.
        /// </summary>
        /// <returns><c>true</c> if valid, <c>false</c> otherwise.</returns>
        /// <exception cref="System.ObjectDisposedException">RequestQueue;Object Disposed: RequestQueue</exception>
        private bool CheckValid()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException("RequestQueue", "Object Disposed: RequestQueue");
            }

            if ( _requestList != null)
                return true;

            return false;

        }

        #region "Dequeue"
        /// <summary>
        /// Dequeues this instance.
        /// </summary>
        /// <returns>Request.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        public Request Dequeue()
        {
            Request request = null;
            try
            {
                _inRequestSemaphore.Wait();
                request = InternalDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }

            return request;
        }

        /// <summary>
        /// Dequeues the specified timeout.
        /// </summary>
        /// <param name="timeout">The timeout.</param>
        /// <returns>Request.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        public Request Dequeue(TimeSpan timeout)
        {
            Request request = null;
            try
            {
                if (!_inRequestSemaphore.Wait(timeout))
                {
                    return null;
                }

                request = InternalDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentOutOfRangeException(nameof(timeout));
            }

            return request;
        }

        /// <summary>
        /// Dequeues the specified cancellation token.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>Request.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="System.OperationCanceledException"></exception>
        public Request Dequeue(CancellationToken cancellationToken)
        {
            Request request = null;
            try
            {
                _inRequestSemaphore.Wait(cancellationToken);
                request = InternalDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }
            catch (OperationCanceledException)
            {
                throw new OperationCanceledException(cancellationToken);
            }

            return request;
        }

        /// <summary>
        /// Dequeues the specified timeout.
        /// </summary>
        /// <param name="timeout">The timeout.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>Request.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="System.OperationCanceledException"></exception>
        public Request Dequeue(TimeSpan timeout, CancellationToken cancellationToken)
        {
            Request request = null;
            try
            {
                if (!_inRequestSemaphore.Wait(timeout, cancellationToken))
                {
                    return null;
                }
                request = InternalDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentOutOfRangeException(nameof(timeout));
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }
            catch (OperationCanceledException)
            {
                throw new OperationCanceledException(cancellationToken);
            }

            return request;
        }
        #endregion "Dequeue"

        #region "Asynchronous Dequeue"
        /// <summary>
        /// Dequeue as an asynchronous operation.
        /// </summary>
        /// <returns>Task&lt;Request&gt;.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        public async Task<Request> DequeueAsync()
        {
            Request request = null;
            try
            {
                await _inRequestSemaphore.WaitAsync();
                request = InternalDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }

            return request;
        }

        /// <summary>
        /// dequeue as an asynchronous operation.
        /// </summary>
        /// <param name="timeout">The timeout.</param>
        /// <returns>Task&lt;Request&gt;.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        /// <exception cref="System.ObjectDisposedException"></exception>
        public async Task<Request> DequeueAsync(TimeSpan timeout)
        {
            Request request = null;
            try
            {
                var t = _inRequestSemaphore.WaitAsync(timeout);
                await t;

                if (!t.Result)
                {
                    return null;
                }

                request = InternalDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentOutOfRangeException(nameof(timeout));
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }

            return request;
        }

        /// <summary>
        /// dequeue as an asynchronous operation.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>Task&lt;Request&gt;.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="System.OperationCanceledException"></exception>
        public async Task<Request> DequeueAsync(CancellationToken cancellationToken)
        {
            Request request = null;
            try
            {
                var t = _inRequestSemaphore.WaitAsync(cancellationToken);
                await t;

                request = InternalDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }
            catch (OperationCanceledException)
            {
                throw new OperationCanceledException(cancellationToken);
            }

            return request;
        }

        /// <summary>
        /// dequeue as an asynchronous operation.
        /// </summary>
        /// <param name="timeout">The timeout.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>Task&lt;Request&gt;.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="System.OperationCanceledException"></exception>
        public async Task<Request> DequeueAsync(TimeSpan timeout, CancellationToken cancellationToken)
        {
            Request request = null;
            try
            {
                var t = _inRequestSemaphore.WaitAsync(timeout, cancellationToken);
                await t;

                if (!t.Result)
                {
                    return null;
                }

                request = InternalDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentOutOfRangeException(nameof(timeout));
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }
            catch (OperationCanceledException)
            {
                throw new OperationCanceledException(cancellationToken);
            }

            return request;
        }
        #endregion "Priority Asynchronous Dequeue"

        #region "Priority Dequeue"
        /// <summary>
        /// Dequeue priority wise.
        /// </summary>
        /// <returns>Request.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        public Request PriorityDequeue()
        {
            Request request = null;
            try
            {
                _inRequestSemaphore.Wait();
                request = InternalPriorityDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }

            return request;
        }

        /// <summary>
        /// Dequeue priority wise.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>Request.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="System.OperationCanceledException"></exception>
        public Request PriorityDequeue(CancellationToken cancellationToken)
        {
            Request request = null;
            try
            {
                _inRequestSemaphore.Wait(cancellationToken);
                request = InternalPriorityDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }
            catch (OperationCanceledException)
            {
                throw new OperationCanceledException(cancellationToken);
            }

            return request;
        }

        /// <summary>
        /// Dequeue priority wise.
        /// </summary>
        /// <param name="timeout">The timeout.</param>
        /// <returns>Request.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        public Request PriorityDequeue(TimeSpan timeout)
        {
            Request request = null;
            try
            {
                if (!_inRequestSemaphore.Wait(timeout))
                {
                    return null;
                }

                request = InternalPriorityDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentOutOfRangeException(nameof(timeout));
            }

            return request;
        }

        /// <summary>
        /// Dequeue priority wise.
        /// </summary>
        /// <param name="timeout">The timeout.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>Request.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="System.OperationCanceledException"></exception>
        public Request PriorityDequeue(TimeSpan timeout, CancellationToken cancellationToken)
        {
            Request request = null;
            try
            {
                if (!_inRequestSemaphore.Wait(timeout, cancellationToken))
                {
                    return null;
                }

                request = InternalPriorityDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }
            catch (OperationCanceledException)
            {
                throw new OperationCanceledException(cancellationToken);
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentOutOfRangeException(nameof(timeout));
            }

            return request;
        }
        #endregion "Priority Dequeue"

        #region "Priority Asynchronous Dequeue"
        /// <summary>
        /// Dequeue priority wise asynchronously.
        /// </summary>
        /// <returns>Request.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        public async Task<Request> PriorityDequeueAsync()
        {
            Request request = null;
            try
            {
                await _inRequestSemaphore.WaitAsync();
                request = InternalPriorityDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }

            return request;
        }

        /// <summary>
        /// Dequeue priority wise asynchronously.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>Request.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="System.OperationCanceledException"></exception>
        public async Task<Request> PriorityDequeueAsync(CancellationToken cancellationToken)
        {
            Request request = null;
            try
            {
                await _inRequestSemaphore.WaitAsync(cancellationToken);
                request = InternalPriorityDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }
            catch (OperationCanceledException)
            {
                throw new OperationCanceledException(cancellationToken);
            }

            return request;
        }

        /// <summary>
        /// Dequeue priority wise asynchronously..
        /// </summary>
        /// <param name="timeout">The timeout.</param>
        /// <returns>Request.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        public async Task<Request> PriorityDequeueAsync(TimeSpan timeout)
        {
            Request request = null;
            try
            {
                var t = _inRequestSemaphore.WaitAsync(timeout);
                await t;
                if (!t.Result)
                {
                    return null;
                }

                request = InternalPriorityDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentOutOfRangeException(nameof(timeout));
            }

            return request;
        }

        /// <summary>
        /// Dequeue priority wise asynchronously..
        /// </summary>
        /// <param name="timeout">The timeout.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>Request.</returns>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="System.OperationCanceledException"></exception>
        public async Task<Request> PriorityDequeueAsync(TimeSpan timeout, CancellationToken cancellationToken)
        {
            Request request = null;
            try
            {
                var t = _inRequestSemaphore.WaitAsync(timeout, cancellationToken);
                await t;
                if (!t.Result)
                {
                    return null;
                }

                request = InternalPriorityDequeue();
                _inRequestSemaphore.Release();
            }
            catch (ObjectDisposedException e)
            {
                throw new ObjectDisposedException(e.ObjectName);
            }
            catch (OperationCanceledException)
            {
                throw new OperationCanceledException(cancellationToken);
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentOutOfRangeException(nameof(timeout));
            }

            return request;
        }
        #endregion "Priority Asynchronous Dequeue"

        #region "IDisposable"
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="dispose"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool dispose)
        {
            if (dispose)
            {
                _requestList?.Clear();
                _inRequestSemaphore.Dispose();
                _outRequestSemaphore.Dispose();
            }

            _disposed = true;
        }
        #endregion "IDisposable"

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
        public IEnumerator<Request> GetEnumerator()
        {
            CheckValid();
            return _requestList.GetEnumerator();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="RequestQueue{TRes}"/> class.
        /// </summary>
        ~RequestQueue()
        {
            Dispose(false);
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins.</param>
        /// <exception cref="System.ArgumentNullException">Arguement can not be null</exception>
        /// <exception cref="System.RankException">Can not copy to multiDimensional array</exception>
        /// <exception cref="System.ArrayTypeMismatchException">Array type mismatch</exception>
        /// <exception cref="System.InvalidCastException">Invalid type of array</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Array index out of range</exception>
        /// <exception cref="System.ArgumentException">Array index out of range</exception>
        public void CopyTo(Array array, int index)
        {
            CheckValid();
            try
            {
                var src = _requestList.ToArray();
                Array.Copy(src, 0, array, index, src.Length);
            }
            catch (ArgumentNullException)
            {
                throw new ArgumentNullException(nameof(array), "Arguement can not be null");
            }
            catch (RankException)
            {
                throw new RankException("Can not copy to multiDimensional array");
            }
            catch (ArrayTypeMismatchException)
            {
                throw new ArrayTypeMismatchException("Array type mismatch");
            }
            catch (InvalidCastException)
            {
                throw new InvalidCastException("Invalid type of array");
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentOutOfRangeException(nameof(array), "Array index out of range");
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Array index out of range", nameof(array));
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        /// <value>The count.</value>
        int ICollection.Count
        {
            get
            {
                CheckValid();
                return _requestList.Count;
            }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        /// <value>The synchronize root.</value>
        /// <exception cref="System.NotSupportedException"></exception>
        public object SyncRoot
        {
            get
            {
                throw new NotSupportedException();
            }
        }

        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        /// <value><c>true</c> if this instance is synchronized; otherwise, <c>false</c>.</value>
        public bool IsSynchronized
        {
            get
            {
                CheckValid();
                return false;
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        /// <value>The count.</value>
        int IReadOnlyCollection<Request>.Count
        {
            get
            {
                CheckValid();
                return _requestList.Count;
            }
        }
    }
}