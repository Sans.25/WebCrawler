﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;

namespace NDownloadManagerTests
{
    [TestClass]
    public class FileTest
    {
        [TestMethod]
        public void GetFileSizeTest()
        {
            NDownloadManagerLib.Base.IO.File file = new NDownloadManagerLib.Base.IO.File()
            {
                Url = @"https://apis.google.com/js/platform.js"
            };
            Assert.IsNotNull(file);
            Assert.IsTrue(file.GetFileSize() > 0);
        }

        [TestMethod]
        public void CreateFragmentTest()
        {
            NDownloadManagerLib.Base.IO.File file = new NDownloadManagerLib.Base.IO.File()
            {
                Url = @"https://apis.google.com/js/platform.js"
            };

            Assert.IsNotNull(file);

            file.GetFileSize();
            // 1 fragment
            file.CreateFragment();

            Assert.IsTrue(file.Fragments.Count == 1);

            Debug.WriteLine(file.Fragments.First().EndByte);
        }


        [TestMethod]
        public void CreateFragmentMultipleTest()
        {
            NDownloadManagerLib.Base.IO.File file = new NDownloadManagerLib.Base.IO.File()
            {
                Url = @"https://apis.google.com/js/platform.js"
            };

            Assert.IsNotNull(file);

            file.GetFileSize();

            // 5 fragment
            file.CreateFragment();
            file.CreateFragment();
            file.CreateFragment();
            file.CreateFragment();
            file.CreateFragment();

            Assert.IsTrue(file.Fragments.Count == 5);

            Debug.WriteLine(file.Fragments.First().EndByte);
        }

        [TestMethod]
        public void DownloadFragmentTest()
        {
            NDownloadManagerLib.Base.IO.File file = new NDownloadManagerLib.Base.IO.File()
            {
                Url = @"https://apis.google.com/js/platform.js"
            };

            Assert.IsNotNull(file);

            // 1 fragment
            file.CreateFragment();

            Assert.IsTrue(file.Fragments.Count == 1);

            Debug.WriteLine(file.Fragments.First().EndByte);

            file.Fragments.First().Download();
            Debug.WriteLine(Path.GetFullPath(file.Filename));
        }

        [TestMethod]
        public void DownloadFragmentMultipleTest()
        {
            NDownloadManagerLib.Base.IO.File file = new NDownloadManagerLib.Base.IO.File()
            {
                Url = @"https://apis.google.com/js/platform.js"
            };

            Assert.IsNotNull(file);

            file.GetFileSize();

            file.CreateFragment();
            file.CreateFragment();
            file.CreateFragment();
            file.CreateFragment();
            file.CreateFragment();

            Assert.IsTrue(file.Fragments.Count == 5);

            Debug.WriteLine(file.Fragments.First().EndByte);
            Debug.WriteLine(Path.GetFullPath(file.Filename));

            file.Fragments.ForEach(f => f.Download());
        }
    }

}
