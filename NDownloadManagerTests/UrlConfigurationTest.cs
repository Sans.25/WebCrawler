﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Xml.Serialization;
using NDownloadManagerLib.Base.Net;
using System.IO;
using System.Diagnostics;
using System.Runtime.Serialization;
using NDownloadManagerLib;
using System.ComponentModel;
using System.Collections.Generic;

namespace NDownloadManagerTests
{
    [TestClass]
    public class UrlConfigurationTest
    {
        [TestMethod]
        public void SerializationTest()
        {
            UrlConfiguration urlConfiguration = new UrlConfiguration()
            {
                Url = @"http://www.google.com/"
            };
            //DataContractSerializer dataContractSerializer = new DataContractSerializer(typeof(UrlConfiguration));
            XmlSerializer xmlSerializer = new XmlSerializer(urlConfiguration.GetType());
            MemoryStream memoryStream = new MemoryStream();
            xmlSerializer.Serialize(memoryStream, urlConfiguration);

            // goto first position..
            memoryStream.Seek(0, SeekOrigin.Begin);

            // debug information
            StreamReader reader = new StreamReader(memoryStream);
            Debug.WriteLine(reader.ReadToEnd());
            //reader.Close();

            // goto first position..
            memoryStream.Seek(0, SeekOrigin.Begin);

            //var deserializedList = dataContractSerializer.ReadObject(memoryStream) as UrlConfiguration;
            var deserializedObject = xmlSerializer.Deserialize(memoryStream) as UrlConfiguration;
            Assert.IsNotNull(deserializedObject);
            Assert.IsTrue(urlConfiguration.Equals(deserializedObject));
        }

        [TestMethod]
        public void SerializationTest2()
        {
            UrlConfiguration urlConfiguration = new UrlConfiguration()
            {
                Url = @"http://www.google.com/"
            };

            NDownloadManagerLib.Base.Net.UrlConfigurationCollection configuration = new NDownloadManagerLib.Base.Net.UrlConfigurationCollection();
            configuration.Add(urlConfiguration);
            urlConfiguration = new UrlConfiguration()
            {
                Url = @"http://www.yahoo.com/"
            };
            configuration.Add(urlConfiguration);

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<UrlConfiguration>));//configuration.GetType());
            MemoryStream memoryStream = new MemoryStream();
            xmlSerializer.Serialize(memoryStream, new List<UrlConfiguration>(configuration));//configuration);

            /*using (*/
            StreamReader reader = new StreamReader(memoryStream)/*)*/;
                Debug.WriteLine(reader.ReadToEnd());

            memoryStream.Seek(0, SeekOrigin.Begin);

            var deserializedList = xmlSerializer.Deserialize(memoryStream) as List<UrlConfiguration>;
            Assert.IsNotNull(deserializedList);
            
            var deserializedCollection = new UrlConfigurationCollection();
            foreach (var item in deserializedList)
            {
                deserializedCollection.Add(item);
            }

            for (int i = 0; deserializedCollection.Count != 0; i++)
            {
                urlConfiguration = configuration.Take();
                var deserializedUrlConfiguration = deserializedCollection.Take();

                Assert.IsNotNull(urlConfiguration);
                Assert.IsNotNull(deserializedUrlConfiguration);

                Assert.IsTrue(urlConfiguration.Equals(deserializedUrlConfiguration));
            }
        }

        /// <summary>
        /// File Save & Load Tests..
        /// </summary>
        [TestMethod]
        public void SerializationTest3()
        {
            NDownloadManagerLib.Configuration configuration = new NDownloadManagerLib.Configuration();
            UrlConfiguration urlConfiguration = new UrlConfiguration()
            {
                Url = @"http://www.google.com/"
            };
            configuration.UrlConfigurations.Add(urlConfiguration);

            urlConfiguration = new UrlConfiguration()
            {
                Url = @"http://www.google.com/search"
            };
            configuration.UrlConfigurations.Add(urlConfiguration);
            configuration.NumberOfThreadsPerFile = 1;
            configuration.NumberOfThreadsPerProject = 256;

            configuration.Save();

            Assert.IsTrue(File.Exists(@"NDownloadManager.conf"));

            // empty configuration
            while (configuration.UrlConfigurations.Count > 0)
                configuration.UrlConfigurations.Take();

            configuration.Load();

            Assert.IsNotNull(configuration.UrlConfigurations);
            Assert.IsTrue(configuration.UrlConfigurations.Count == 2);
            Assert.IsTrue(configuration.UrlConfigurations.Take().Url.Equals(@"http://www.google.com/"));
            Assert.IsTrue(configuration.UrlConfigurations.Take().Url.Equals(@"http://www.google.com/search"));
            Assert.IsTrue(configuration.NumberOfThreadsPerFile == 1);
            Assert.IsTrue(configuration.NumberOfThreadsPerProject == 256);
        }

        //TODO CookieManager test..
    }
}
