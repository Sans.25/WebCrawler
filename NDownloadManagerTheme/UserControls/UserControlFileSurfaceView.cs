﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NDownloadManagerTheme.UserControls
{
    public partial class UserControlFileSurfaceView : UserControl
    {
        public const ulong KiloByte = 1024;
        public const ulong MegaByte = KiloByte * KiloByte;
        public const ulong GigaByte = KiloByte * KiloByte * KiloByte;
        public const ulong TerraByte = KiloByte * KiloByte * KiloByte * KiloByte;

        public const ulong BlockSize = 1024;
        public const ulong SectorSize = 512;

        public readonly char FILL_CHAR = '■';
        public readonly char EMPTY_CHAR = '□';
        public readonly char CURRENT_PROGRESS_CHAR = '○';
        public readonly char CURRENT_FILL_CHAR = '●';
        //●○

        private char[] data = null;
        public UserControlFileSurfaceView()
        {
            InitializeComponent();
        }

        public void FillData(ulong currentSize, ulong maxSize)
        {
            ulong nSectors = maxSize / SectorSize;
            ulong nBlocks = maxSize / BlockSize;

            textBoxData.Text = string.Empty;
            ulong nFilledBlocks = currentSize / BlockSize;

            ulong dataSize = nBlocks + ((maxSize % BlockSize > 0) ? (ulong)1 : (ulong)0);
            if (data == null || (ulong)data.Length < dataSize)
            {
                data = new char[dataSize];
            }

            for (ulong i = 0; i < dataSize; i++)
            {
                data[i] = (i <= nFilledBlocks ? FILL_CHAR : EMPTY_CHAR);

                if (i == nFilledBlocks + 1 && currentSize % BlockSize >= 0)
                    data[i] = CURRENT_PROGRESS_CHAR;
                else if (i == nFilledBlocks && currentSize % BlockSize == 0)
                    data[i] = CURRENT_FILL_CHAR;
            }

            string dataString = new string(data);
            textBoxData.Text = dataString;
            GC.Collect();
        }
    }
}
