﻿namespace NDownloadManagerTheme.UserControls
{
    partial class Button
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelButton = new System.Windows.Forms.Label();
            this.flowLayoutPanelBackground = new System.Windows.Forms.FlowLayoutPanel();
            this.panelButton = new System.Windows.Forms.Panel();
            this.flowLayoutPanelBackground.SuspendLayout();
            this.panelButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelButton
            // 
            this.labelButton.AutoSize = true;
            this.labelButton.Location = new System.Drawing.Point(14, 5);
            this.labelButton.Name = "labelButton";
            this.labelButton.Size = new System.Drawing.Size(57, 20);
            this.labelButton.TabIndex = 0;
            this.labelButton.Text = "Button";
            // 
            // flowLayoutPanelBackground
            // 
            this.flowLayoutPanelBackground.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanelBackground.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanelBackground.Controls.Add(this.panelButton);
            this.flowLayoutPanelBackground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelBackground.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanelBackground.Name = "flowLayoutPanelBackground";
            this.flowLayoutPanelBackground.Size = new System.Drawing.Size(92, 37);
            this.flowLayoutPanelBackground.TabIndex = 1;
            // 
            // panelButton
            // 
            this.panelButton.Controls.Add(this.labelButton);
            this.panelButton.Location = new System.Drawing.Point(3, 3);
            this.panelButton.Name = "panelButton";
            this.panelButton.Size = new System.Drawing.Size(87, 32);
            this.panelButton.TabIndex = 1;
            // 
            // Button
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.flowLayoutPanelBackground);
            this.Name = "Button";
            this.Size = new System.Drawing.Size(92, 37);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Button_Paint);
            this.flowLayoutPanelBackground.ResumeLayout(false);
            this.panelButton.ResumeLayout(false);
            this.panelButton.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelButton;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelBackground;
        private System.Windows.Forms.Panel panelButton;
    }
}
