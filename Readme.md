---
title: NDownloadManager
---

Projects Involved
=================

NDownloadManagerLib \[CLR Class Library\]
-----------------------------------

### Project Specifications

#### Target Platform Specifications

  ----------------------- ----------------------
  Project Type            CLRClass Library
  Target .Net Framework   Microsoft .Net 4.5.2
  Target Platform         Microsoft Windows 7
  ----------------------- ----------------------

#### Requirement Specifications

1.  Should be able to handle as many types of files as possible.

    a.  Document Type Handler

        i.  HTML, JS, CSS for a start

        ii. Must be able to identify Document Type from downloaded
            content or through online support

2.  Protocol Supports

    a.  HTTP Support

    b.  HTTP Tunneling support

    c.  FTP Support

    d.  FTPS Support

    e.  HTTPS Support

3.  Should be able to multi-thread downloads

    a.  Multi-threaded downloads using multiple proxy servers must be
        supported

4.  Multiple Domain handling along with Cookies \[Includes 3^rd^ Party
    Cookies\] & Logins

    a.  Fully customizable domain configurations

        i.  Settings to include or exclude:

            -   Cookies

                a.  Includes 3^rd^ Party Cookies

            -   Logins

                a.  Use Login per Proxy or Use Login using 1 IP only.

            -   Support Default Protocols only \[or Selectable\]

                a.  Non-Default Protocols would be supported in case
                    selected as default

                b.  Non-Default Protocols would be supported in case
                    selected for domain configuration

5.  Proxy Support

    a.  HTTP Proxy

    b.  HTTPS Proxy

    c.  SOCKS 4 Proxy

    d.  SOCKS 5 Proxy

6.  Certification Support

    a.  SSL support

    b.  TLS support

7.  Login Management

    a.  Storage of passwords

8.  Filters

    a.  IP Based filters

    b.  Domain Based filters

    c.  Document Based filters

9.  Plugins Support

    a.  Plugin Creation and Inclusion support

    b.  Other protocol support

    c.  Other File Type / Document Type support

#### License Specifications

-   Open source project

    -   MIT License would suffice

#### Library Inclusion Specifications

-   HtmlAgilityPack for HTML Handling \[Initial Import\]

    -   Under Ms-PL

-   Jint for Javascript Parsing

    -   Under MIT License

-   Microsoft.Owin.Security.Cookies

    -   Under Microsoft Software License Terms

    -   For Cookie based authentication support

#### Software Class Library Design Specifications

##### ConnectionManager

-   Class responsible for handling all of the ongoing connections in:

    -   Project

        -   Per project Connection limit is kept and maintained

    -   Global Scope

        -   A Global Connection limit is kept and maintained

-   Must keep track of:

    -   Protocols

    -   Document Types Involved

    -   Plugins used

        -   Plugins generated files

        -   Plugins generated protocols

    -   Connection count per server:

        -   Through base IP

        -   Through Proxy

-   Before a ProxyManager starts to open a connection ConnectionManager
    is considered for validation of active connection count of:

    -   Target Server

    -   Active Connection count of Proxy

        -   This limits the Proxy Usage

##### ProxyManager

-   Class responsible for:

    -   Keeping track of Proxy Server lists:

        -   Proxy Online Status

            -   Active Proxy

            -   Inactive Proxy

        -   Proxy Anonymity Level

            -   Anonymous Proxy

            -   Fully Anonymous Proxy

        -   Proxy Type:

            -   HTTP Proxy

            -   HTTPS Proxy

            -   Socks Proxy

                -   Socks 4 Proxy

                -   Socks 5 Proxy

        -   Proxy Latency

        -   Proxy Data Rate

            -   Calculated on first connection usage

            -   Can be manually calculated

##### LoginManager

-   Main Purposes:

    -   Store Logins \[Users, Passwords, Sessions, Cookies\]

    -   Storage master password

    -   Multiple IP records for active Logins in *ConnectionManager*:

        -   per Project

        -   Global

##### DocumentManager

-   Main Purposes:

    -   Document Identifier

        -   Must identify document type through:

            -   MIME type checking

            -   Document Forensics

            -   Online lookups

            -   Plugins supports

    -   Document Manager

        -   Must actively identify if document should be downloaded

            -   Based upon Global Settings

            -   Based upon Domain policy for Project involved

            -   Based upon Filters

                -   IP based restrictions / filters

                -   Domain based filters

                -   Document type based filters

        -   Must actively identify downloadable links in document
            (if any)

##### DownloadQueueManager

-   Basically, all the downloads are queued into this:

    -   Global Download Queue

    -   Per Project Download Queue

-   Multiple threads can download the Downloads using Synchronous ways

-   Download File Information Stored:

    -   Download URL

        -   Parallel Download URLs

            -   Non Erroneous

    -   Download Protocol

    -   IsFiltered

        -   Project based

            -   Domain based

            -   Depth based

        -   IP based

        -   Filter based

    -   Project Involved

##### FIFOQueueManager

-   Required for handling of Global and Project based FIFO queues

-   Must have unified queue internally

-   Should return filtered elements in case of project based

    -   Should support MultiThreading for:

        -   Project

        -   Global

##### ProjectManager

-   Project for storing information:

    -   Project name

    -   Description

    -   Base URL

    -   Base Protocol

    -   NDownloadManager Depth

    -   Filter Options for Project

    -   Domain Options for Project

    -   Proxy List in case separate from Global

    -   Connection Limits

##### FilterOptionsManager

-   Filter Scopes:

    -   Global

    -   Project based

-   Filter Types:

    -   IP based

    -   Domain based

    -   Document based

##### DomainManager

-   Domain Options Scopes:

    -   Global

    -   Project based

-   Domain Options:

    -   Cookies

        -   3^rd^ Party Cookies

    -   Session Headers

    -   User Login

        -   Support Proxy?

        -   Support MultiLogin

    -   Connection Count:

        -   Global
